<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    protected $fillable = ['name', 'surname','nick_name','year_born','gender', 'height', 'weight', 'position', 'photo', 'gallery_id'];
}

