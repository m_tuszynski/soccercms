<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Match extends Model

{
    protected $fillable = ['title', 'body','data','league_id','localisation_id', 'referee_id', 'gallery_id', 'article_id'];


    public function squad()
    {
        return $this->hasmany(Squad::class);

    }
}
