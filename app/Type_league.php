<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type_league extends Model
{
    protected $fillable = ['name', 'season_id'];
}
