<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Club extends Model
{

    protected $fillable = ['name', 'short_name','nick_name','color','embed', 'photo', 'captain', 'vice_captain', 'gallery_id'];

    public function player()
    {
        return $this->hasmany(Player::class);

    }

    public function fixture()
    {
        return $this->belongsToMany(Fixture::class);

    }

}
