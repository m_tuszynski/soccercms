<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Squad extends Model
{

    protected $fillable = ['club_id'];


    public function player()
    {
        return $this->hasmany(Player::class);

    }
}

