<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trophie extends Model
{
    protected $fillable = ['name', 'club_id','player_id','league_id'];
}
