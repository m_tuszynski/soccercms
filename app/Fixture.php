<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fixture extends Model
{
    protected $fillable = ['data', 'number_fixtures','league_id','season_id'];

    public function club()
    {
        return $this->belongsToMany(Club::class);
    }
}
