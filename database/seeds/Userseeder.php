<?php

use Illuminate\Database\Seeder;

class Userseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User();
        $user->name ='Mirek';
        $user->email ='admin@admin.pl';
        $user->password =bcrypt('admin');
        $user->save();
    }
}
