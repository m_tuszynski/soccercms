<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('details', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('typ', ['YELLOW_CARD', 'RED_CARD', 'GOAL', 'OWN_GOAL', 'PENALTY_SCORED', 'PENALTY_MISSED']);

            $table->integer('match_id')->unsigned();
            $table->foreign('match_id')->references('id')->on('matches');

            $table->integer('player_id')->unsigned();
            $table->foreign('player_id')->references('id')->on('players');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('details');
    }
}
