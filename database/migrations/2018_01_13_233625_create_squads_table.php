<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSquadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('squads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('club_id')->unsigned();
            $table->foreign('club_id')->references('id')->on('clubs');
            $table->timestamps();
        });

        Schema::create('player_squad', function (Blueprint $table) {
            $table->integer('player_id')->unsigned();
            $table->integer('squad_id')->unsigned();

            $table->foreign('player_id')->references('id')->on('players');
            $table->foreign('squad_id')->references('id')->on('squads');
        });

        Schema::create('match_squad', function (Blueprint $table) {
            $table->integer('match_id')->unsigned();
            $table->integer('squad_id')->unsigned();

            $table->foreign('match_id')->references('id')->on('matches');
            $table->foreign('squad_id')->references('id')->on('squads');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('squads');
        Schema::dropIfExists('player_squad');
        Schema::dropIfExists('match_squad');
    }
}
