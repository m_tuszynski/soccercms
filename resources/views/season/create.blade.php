@extends('layouts.dashboard')
@section('title', 'Lista sezonów')
@section('page_heading')
    Dodaj sezon
@endsection

@section('section')

{!! Form::open(['route' => 'seasons.store', 'class' => 'form-group']) !!}

{!! Form::label('date', 'data sezonu:') !!}
{!! Form::date('date', null, ['placeholders' => 'data...']) !!}
{!! Form::label('archive', 'Archiwum:') !!}
{!! Form::text('archive', null, ['placeholders' => 'Czy archiwum...']) !!}
<button class="btn btn-info btn-circle"><i class="fa fa-check"></i></button>
    {!! Form::close() !!}



@endsection