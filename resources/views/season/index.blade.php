@extends('layouts.dashboard')
@section('title', 'Lista sezonów')
@section('page_heading')
    Zarzadzanie sezonami
@endsection

@section('section')

    <a href="{{route('seasons.create')}}" class="btn btn-primary">Dodaj</a>

            <table class="table table-hover">
                <tr>
                    <td>Id</td>
                    <td>Date</td>
                    <td>Edit</td>
                    <td>Delete</td>
                </tr>
            @foreach($season as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->date}}</td>
                    <td><a href="{{route('seasons.edit', $item)}}" class="btn btn-primary">Edytuj</a></td>
                    <td><a href="{{route('seasons.create')}}" class="btn btn-primary">Usuń</a></td>
                </tr>

            @endforeach

            </table>



@endsection