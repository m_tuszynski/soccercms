@extends('layouts.dashboard')
@section('title', 'Edytuj sezon')
@section('page_heading')
    Edycja sezonów
@endsection

@section('section')

{!! Form::open(['route' => ['seasons.update', $seasons], 'method' => 'put']) !!}

{!! Form::label('date', 'data sezonu:') !!}
{!! Form::date('date', $seasons->date) !!}
{!! Form::label('archive', 'Archiwum:') !!}
{!! Form::text('archive', $seasons->archive) !!}
<button class="btn btn-info btn-circle"><i class="fa fa-check"></i></button>
    {!! Form::close() !!}



@endsection